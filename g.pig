-- some comment 
SET default_parallel $reducers;

lineitem = load '$input/lineitem' USING PigStorage('|') as (l_orderkey, l_partkey,
l_suppkey, l_linenumber, l_quantity, l_extendedprice, l_discount, l_tax, l_returnflag, l_linestatus, l_shipdate, l_commitdate, l_receiptdate,l_shippingstruct, l_shipmode, l_comment);

flineitem = FILTER lineitem BY l_shipdate >= '1994-01-01' AND l_shipdate < '1995-01-01' AND l_discount >= 0.05  AND l_discount <= 0.07 AND l_quantity < 24;

saving2 GENERATE l_extendedprice * l_discount;
grpResult = GROUP saving2 ALL;
sumResult = FOREACH grpResult GENERATE SUM(saving2);

store sumResult into '$output/Q6out' USING PigStorage('|');

-- Generate with basic arithmetic
asd = LOAD '/user/bj112/data/1/dataset_30000' USING PigStorage('\t') 
	AS (name:chararray, age:int, gpa:float) PARALLEL 8;
someVariableName = 	FOREACH asd 
	GENERATE age * gpa + 3, age/gpa - 1.5 PARALLEL 8;
STORE someVariableName INTO 'dataset_30000' USING PigStorage() PARALLEL 8;

-- Generate with basic arithmetic
asd = LOAD '/user/bj112/data/1/dataset_30000' USING PigStorage('\t') 
	AS (name:chararray, age:int, gpa:float) PARALLEL 8;
someVariableName = 	FOREACH asd 
	GENERATE age * gpa + 3, age/gpa - 1.5 PARALLEL 8;
STORE someVariableName INTO 'dataset_30000' USING PigStorage() PARALLEL 8;